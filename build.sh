#!/bin/bash

SOURCE=src/main.cr
OUT=bin/dictnorm

#./gen_version.sh

crystal build $SOURCE -o $OUT \
--release \
--verbose \
--link-flags "-flto"
