# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

require "cuid"
require "xml"
require "./types.cr"
require "./markers.cr"
require "./asciifolder.cr"

include Types
include Markers
include ASCIIFolder

module Dictnorm1
  extend self

  def normalize_authors(markers, source, wrap = true)
    authors = markers.authors
    wraps = markers.wraps
    authors.keys.reduce(source) { |text, k|
      s = wrap ? wrap_source(wraps["author"], authors["#{k}"]) : authors["#{k}"]
      text.gsub(/\s#{k}[\s\.\)]/, " #{s} ")
    }
  end

  def normalize_annots(markers, source, wrap = true)
    annots = markers.annots
    wraps = markers.wraps
    annots.keys.reduce(source) { |text, k|
      s = wrap ? wrap_source(wraps["annot"], annots["#{k}"]) : annots["#{k}"]
      text.gsub(/\s#{k}[\s\.\)]/, " #{s} ")
    }
  end

  def normalize_substs(substs, source)
    substs.keys.reduce(source) { |text, k|
      text.gsub(k, substs["#{k}"])
    }
  end

  def squeeze_str(source)
    source.squeeze(".")
      .squeeze(" ")
  end

  def normalize_chars(markers, source)
    s = normalize_substs(markers.chars_subst, source)
    s.strip
  end

  def normalize_words(markers, source)
    s = normalize_substs(markers.subst, source)
    s.strip
  end

  def normalize_stops1(source)
    source.gsub(/\.$/im, ".\n\n")
  end

  def normalize_stops2(source)
    s = source.gsub(" .", ".")
    s.strip
  end

  def push_left_lines(source)
    source.gsub(/^[ ]+/im, "")
  end

  def normalize_text(source)
    cr = "[.>>]"
    cr2 = "[.|>]"
    stop = "[.>]"
    spc = "[_]"
    ellipsis = "[.3]"
    s = source.gsub(/-\n+/, "")
    # s = s.gsub(/\.\n+(?<v>\w+\s[^=])/, ". \\k<v>") # ??? Resolved special case?
    # s = s.gsub(/\.\n+(?<v>\w+\s[^:=→])/, ". \\k<v>") # ??? Resolved special case?
    s = s.gsub(/\.\.\./, ellipsis)
    s = s.gsub(/\d+\.\s*/, "") # all spaces???
    # s = s.gsub(/(?<v>[^aeiou]\.)\n/, "\\k<v>#{stop}") #problematic in some case ???
    s = s.gsub(/\.\n\n/, cr)
    # s = s.gsub(/\.\n/, cr2) # very special case 1 ???
    s = s.gsub(/\n\n/, spc)
    s = s.gsub(/\n/, spc)
    s = s.gsub(cr, ".\n\n")
    # s = s.gsub(cr2, ".\n\n") # restore very special case 1 ???
    s = s.gsub(spc, " ")
    s = s.gsub(stop, " ")
    s = s.gsub(ellipsis, "...")
    s.strip
  end

  def normalize_phases(markers, source, phases)
    phs = phases.map { |ph| ph.downcase }
    s = source
    s = normalize_stops1(source)
    s = normalize_text(s) if phs.includes?("text")
    s = normalize_chars(markers, s) if phs.includes?("chars")
    s = normalize_words(markers, s) if phs.includes?("words")
    normalize_stops2(s)
  end

  def normprocess_text(markers, source)
    s = normalize_authors(markers, source)
    s = normalize_annots(markers, s)
    s = s.gsub(/ \n\n/, ".\n\n")
    # s = s.gsub(/\.\s+/, ".\n\n") # ??? test
    s.strip
  end

  def normadapt_text(source)
    # source = source.gsub("ĭi", "#") # FIXME: Restore # from prior conversion
    "\n\n" + squeeze_str(source.strip + ".") + "\n\n"
  end

  def normadapt_lemmas(source,
                       filters : Hash(String, {String, String, Array(String)}) = nil,
                       filter_compare = Types::CompareFlags::Default)
    r = /^(?<lemma>.*?)[ ,]/im
    mlemmas = source.scan(r)
    mlemmas.each { |mlemma|
      lemma = mlemma["lemma"].strip
      rlemma = ASCIIFolder.replacing(lemma, "_")
      any, flemma = Utils.strings_includes(rlemma, filters.keys, filter_compare)
      if any
        _cat, lword, _subcats = filters["#{flemma}"]
        source = source.gsub(lemma, lword)
      end
    }
    source
  end

  private def get_lemma(source)
    source.split(",")[0]
  end

  def reduce_lemmas_chain(
    source : String,
    fname : String,
    filters : Hash(String, {String, String, Array(String)}) = nil,
    filter_compare = Types::CompareFlags::Default
  )
    r = /^\d*\.*([^:=→\n]+)[:=→]/imx
    rlemmas = source.scan(r)
    r = rlemmas.reduce([] of {String, String, Array(String)}) { |memo, l|
      md1 = l[1].strip
      unless filters.empty?
        begin
          lemma = ASCIIFolder.replacing(get_lemma(md1), "_")
          # memo << md1 if filters.includes?(lemma)
          any, flemma = Utils.strings_includes(lemma, filters.keys, filter_compare)
          cat, _lword, subcats = filters["#{flemma}"]
          memo << {cat, md1, subcats} if any
        rescue ex
          if lemma.nil?
            errors = [
              "Filters error!",
              "Malformed source: #{fname}",
              "Lemma: #{l[1]}",
              ex.message,
            ]
            Utils.exit_error(errors.join("\n"))
          end
          if lemma.empty?
            errors = [
              "Filters error!",
              "Lemma is empty!",
              "Malformed source: #{fname}",
              "Lemma: #{l[1]}",
              ex.message,
            ]
            Utils.exit_error(errors.join("\n"))
          end
        end
      else
        memo << {"", md1, [] of String}
      end
      memo
    }
  end

  def normalize_lemmas(lemma)
    parts = lemma.split(" ")
    parts
  end

  def reduce_lemmas(markers, lemmas, source)
    lemmas.reduce([] of Tuple(String, String, Array(String), String)) do |memo, tlemma|
      cat, lemma, subcats = tlemma
      lemma_sep = markers.splitters["lemma"]
      r = /#{Regex.escape(lemma)}\s*([:=→])(.*)\./
      voc = source.match(r)
      unless voc.nil?
        # t = voc.captures[0].to_s
        # text = voc.captures[1].to_s
        # text = "#{t} #{text}" unless t == ":"
        # memo << {lemma.strip, text.strip}
        t = voc.captures[0].to_s
        text = voc.captures[1].to_s
        text = "#{t} #{text}" unless t == ":"
        rsep = /\s+#{lemma_sep}\s+/
        if lemma.match(rsep)
          slemmas = lemma.split(rsep)
          slemmas.each { |slemma| memo << {cat, slemma.strip, subcats, text.strip} }
        else
          memo << {cat, lemma.strip, subcats, text.strip}
        end
      end
      memo
    end
  end

  # Regex for find x) point
  # (?=(\s[a-z]\))(.*?)((\s[a-z]\)\s)|\.\n))

  def split_description(markers, descr)
    separators = markers.separators
    splitters = markers.splitters
    descrs = descr.scan(Markers.regex_by_sep(separators)).map { |s| s[1].strip }
    descrs.reduce({
      default: [] of String,
      accent:  [] of String,
      locuz:   [] of String,
      see:     [] of String,
      equal:   [] of String,
      pgram:   [] of String,
    }) do |memo, descr|
      unless descr.empty?
        # first = descr[0].to_s
        #TODO: split for || ???
        parts = descr.split(splitters[:defs]).map(&.strip)
        parts.each do |descr|
          first = descr[0].to_s
          case first
          when separators[:accent]
            memo[:accent].push(descr[1..-1].strip)
          when separators[:locuz]
            memo[:locuz].push(descr[1..-1].strip)
          when separators[:see]
            memo[:see].push(descr[1..-1].strip)
          when separators[:equal]
            memo[:equal].push(descr[1..-1].strip)
          when separators[:pgram]
            memo[:pgram].push(descr[1..-1].strip)
          else
            memo[:default].push(descr.strip)
          end
        end
      end
      memo
    end
  end

  def structurize(markers, lemma_descrs)
    lemma_descrs.reduce([] of LemmaStruct) do |memo, lemma_descr|
      cat, lemma, subcats, descr = lemma_descr
      # lparts = lemma.split(",").map { |l| l.strip }
      lparts = lemma.split(",").reduce([] of String) { |memo, l|
        subp = l.scan(/\w+\./)
        if subp.empty?
          memo << l.strip
        else
          memo += subp.map { |m| m[0].strip }
        end
      }
      data = {
        category:      cat,
        subcategories: subcats,
        lemma:         lparts[0],
        lparts:        lparts,
        descr:         split_description(markers, descr),
      }
      memo << data
      memo
    end
  end

  def structurize_sources(markers, lemmas)
    symbols = markers.symbols
    lemmas.reduce([] of {String, String, Array(String), String}) do |memo, lemma_descr|
      cat, head, subcats, descr = lemma_descr
      lemma = head.split(",")[0]
      memo << {cat, lemma, subcats, "#{lemma}\n#{head}\n#{descr}\n\n"}
    end
  end

  def filter_sources(sources)
    sources.reduce([] of String) { |memo, source|
      memo << source[3]
    }
  end

  def filter_lemma(markers, parts)
    grams = markers.grams
    parts.reduce({[] of String, [] of String}) { |memo, part|
      if grams.includes?(part)
        memo[1] << part
      else
        memo[0] << part
      end
      memo
    }
  end

  private def apply_metamark(xml, t, function = "", text = "")
    xml.element(t["metamark"]["tag"],
      Utils.attrs_h(
        t["sense"]["attrs"] + [["function", function]])) {
      xml.text(text.strip)
    }
  end

  def structurize_teilexs(markers, structure)
    t = markers.xmltei
    su = markers.suffix
    fu = markers.function
    pr = markers.prefix

    ids = structure.reduce(
      Hash(String, String).new(structure.size) { |h, k| h[k] = "unknown" }
    ) { |memo, lemma|
      memo[lemma[:lemma]] = "#{pr["lem"]}.#{CUID.random}"
      memo
    }

    structure.reduce([] of {String, String, Array(String), String}) { |memo, lemma|
      orths, grams = filter_lemma(markers, lemma[:lparts])
      memo << {
        lemma[:category],
        lemma[:lemma],
        lemma[:subcategories],
        XML.build_fragment(indent: "  ") { |xml|
          uuid = ids[lemma[:lemma]]
          xml.element(t["entry"]["tag"],
            Utils.attrs_h(t["entry"]["attrs"] + [["xml:id", uuid]])) {
            xml.element(t["lbl"]["tag"],
              Utils.attrs_h(t["lbl"]["attrs"] + [["ana", "head"]])) {
              xml.text(lemma[:lemma])
            }

            xml.element(t["form"]["tag"],
              Utils.attrs_h(t["form"]["attrs"] + [["type", "taxonomy"]])
            ) {
              xml.element(t["lbl"]["tag"],
                Utils.attrs_h(t["form"]["attrs"] + [["type", "category"]])) {
                xml.text(lemma[:category])
              }

              lemma[:subcategories].each { |cat|
                xml.element(t["lbl"]["tag"],
                  Utils.attrs_h(t["form"]["attrs"] + [["type", "category"]])) {
                  xml.text(cat)
                }
              }
            }

            xml.element(t["form"]["tag"],
              Utils.attrs_h(t["form"]["attrs"] + [["type", "lemma"]])) {
              orths.each { |part|
                xml.element(t["orth"]["tag"],
                  Utils.attrs_h(t["orth"]["attrs"])) {
                  xml.text(part)
                }
              }
            }
            xml.element(t["gramGrp"]["tag"],
              Utils.attrs_h(t["gramGrp"]["attrs"])) {
              grams.each { |part|
                xml.element(t["gram"]["tag"]) {
                  xml.text(part)
                }
              }
            }
            xml.element(t["sense"]["tag"],
              Utils.attrs_h(
                t["sense"]["attrs"] +
                [["xml:id", "#{uuid}.#{su["nor"]}"]])) {
              apply_metamark(xml, t, fu["nor"])
              lemma[:descr][:default].each { |value|
                description_to_xml(markers, xml, value, :nor)
              }
            }
            xml.element(t["sense"]["tag"],
              Utils.attrs_h(
                t["sense"]["attrs"] +
                [["xml:id", "#{uuid}.#{su["acc"]}"]])) {
              apply_metamark(xml, t, fu["acc"])
              lemma[:descr][:accent].each { |value|
                description_to_xml(markers, xml, value, :acc)
              }
            }
            xml.element(t["sense"]["tag"],
              Utils.attrs_h(
                t["sense"]["attrs"] +
                [["xml:id", "#{uuid}.#{su["loc"]}"]])) {
              apply_metamark(xml, t, fu["loc"])
              lemma[:descr][:locuz].each { |value|
                description_to_xml(markers, xml, value, :loc)
              }
            }
            xml.element(t["sense"]["tag"],
              Utils.attrs_h(
                t["sense"]["attrs"] +
                [["xml:id", "#{uuid}.#{su["pgr"]}"]])) {
              apply_metamark(xml, t, fu["pgr"])
              lemma[:descr][:pgram].each { |value|
                description_to_xml(markers, xml, value, :pgr)
              }
            }
            xml.element(t["sense"]["tag"],
              Utils.attrs_h(
                t["sense"]["attrs"] +
                [["xml:id", "#{uuid}.#{su["see"]}"]])) {
              apply_metamark(xml, t, fu["see"])
              lemma[:descr][:see].each { |value|
                description_to_xml(markers, xml, value, :see, ids)
              }
            }
            xml.element(t["sense"]["tag"],
              Utils.attrs_h(
                t["sense"]["attrs"] +
                [["xml:id", "#{uuid}.#{su["equ"]}"]])) {
              apply_metamark(xml, t, fu["equ"])
              lemma[:descr][:equal].each { |value|
                description_to_xml(markers, xml, value, :equ, ids)
              }
            }
          }
        },
      }
    }
  end

  def filter_teilexs(teilexs)
    teilexs.reduce([] of String) { |memo, teilex|
      memo << teilex[3]
    }
  end
end
