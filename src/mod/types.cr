# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

module Types
  alias AStrings = Array(String)
  alias AAStrings = Array(AStrings)
  alias AStringsTuple = {AStrings, AStrings}
  alias AAStringsTuple = {AStrings, AAStrings}
  alias LemmaStruct = {category: String, subcategories: Array(String), lemma: String, lparts: Array(String), descr: {default: Array(String), accent: Array(String), locuz: Array(String), see: Array(String), pgram: Array(String), equal: Array(String)}}
  alias LemmaSource = {String, String, Array(String), String} | LemmaStruct
  alias TagStruct = NamedTuple(tag: String, attrs: AAStrings)
  # alias XmlTeiStruct = Hash(String, TagStruct)
  alias XmlTeiStruct = NamedTuple(
    lbl: TagStruct,
    taxonomy: TagStruct,
    category: TagStruct,
    catDesc: TagStruct,
    div: TagStruct,
    dictScrap: TagStruct,
    note: TagStruct,
    entry: TagStruct,
    head: TagStruct,
    form: TagStruct,
    orth: TagStruct,
    gramGrp: TagStruct,
    gram: TagStruct,
    sense: TagStruct,
    def_: TagStruct,
    span: TagStruct,
    cit: TagStruct,
    cit_translation: TagStruct,
    aquote: TagStruct,
    tquote: TagStruct,
    bibl: TagStruct,
    title: TagStruct,
    author: TagStruct,
    publisher: TagStruct,
    date: TagStruct,
    biblScope: TagStruct,
    xr: TagStruct,
    ref: TagStruct,
    metamark: TagStruct)
  alias GramsStruct = Array(String)
  alias CharsSubstStruct = Hash(String, String)
  alias SubstStruct = Hash(String, String)
  alias AnnotsStruct = Hash(String, String)
  alias PrefixStruct = NamedTuple(lem: String)
  alias SuffixStruct = NamedTuple(nor: String, acc: String, loc: String, see: String, pgr: String, equ: String)
  alias FunctionStruct = NamedTuple(nor: String, acc: String, loc: String, see: String, pgr: String, equ: String)
  alias DocumentStruct = NamedTuple(title: String, publisher: String)
  alias SymbolsStruct = NamedTuple(block: String, sep: String, cit: String)
  alias WrapsStruct = NamedTuple(bracket: Tuple(String, String), round: Tuple(String, String), curly: Tuple(String, String), annot: Tuple(String, String), author: Tuple(String, String))
  alias TagsStruct = NamedTuple(cit: Tuple(String, String), acit: Tuple(String, String), tcit: Tuple(String, String), auth: Tuple(String, String))
  alias SeparatorsStruct = NamedTuple(accent: String, locuz: String, see: String, pgram: String, equal: String)
  alias SplittersStruct = NamedTuple(defs: String, lemma: String)
  alias AuthorsStruct = Hash(String, String)
  alias AuthorResp = String
  alias AuthorDatum = NamedTuple(sname: String, name: String, from: String, to: String, id: String, refname: String)
  alias AuthorsData = Hash(String, AuthorDatum)

  enum CompareFlags
    Default  = 0
    Upcase   = 1
    Downcase = 2
  end
end
