# Copyright (c) 2025 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

@[Link("xml2", pkg_config: "libxml-2.0")]
{% if compare_versions(Crystal::VERSION, "1.11.0-dev") >= 0 %}
  @[Link(dll: "libxml2.dll")]
{% end %}
lib LibXML
  fun xmlTextWriterWriteElement(TextWriter, name : UInt8*, content : UInt8*) : Int
  fun xmlTextWriterWriteRaw(TextWriter, content : UInt8*) : Int
end

class XML::Builder
  def element_with_text(name : String, content : String) : Nil
    call WriteElement, string_to_unsafe(name), string_to_unsafe(content)
  end

  def text_raw(content : String) : Nil
    call WriteRaw, content
  end
end
