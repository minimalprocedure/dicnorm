# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

require "file_utils"
require "./xml_serializer"

module Utils
  extend self

  VERSION = "0.1.0"

  def append_dir(base, dir)
    dir = File.join(base, dir)
    FileUtils.mkdir_p(dir)
    File.expand_path(dir)
  end

  def insert_dir(filename, dir)
    dirname = append_dir(File.dirname(filename), dir)
    File.join(dirname, File.basename(filename))
  end

  def open_source(file)
    File.read(file)
  end

  def save_source(file : String, source : String, dirpath : Nil | String = nil, verbose = false)
    fname = dirpath.nil? ? File.expand_path(file) : insert_dir(file, dirpath)
    Utils.print_info("Save source: #{fname}", verbose)
    File.write(fname, source)
    fname
  end

  def exit_error(msg, code = 1)
    STDERR.puts("ERROR: #{msg}")
    exit(code)
  end

  def print_error(msg)
    STDERR.puts("ERROR: #{msg}")
  end

  def print_info(message, verbose)
    STDOUT.puts(message) if verbose
  end

  def attrs_h(attrs) : Hash(String, String)
    fn = ->(memo : Hash(String, String), a : Types::AStrings) {
      memo["#{a[0]}"] = a[1] if a.size == 2
      memo
    }
    attrs.reduce(Hash(String, String).new, &fn)
  end

  def xmlhead(xmlstr)
    %(<?xml version="1.0" encoding="UTF-8"?>\n#{xmlstr}).strip
  end

  def xmlhead_root(xmlstr)
    %(<?xml version="1.0" encoding="UTF-8"?>\n<body>\n#{xmlstr}\n</body>).strip
  end

  def category_as_path(cat : String)
    cat.split(".").reduce("") { |memo, part|
      memo = File.join(memo, part)
    }
  end

  def strings_includes(s, filters, flag)
    flemma = ""
    any = filters.any? { |lemma|
      flemma = lemma
      case flag
      when Types::CompareFlags::Upcase
        lemma.upcase == s.upcase
      when Types::CompareFlags::Downcase
        lemma.downcase == s.downcase
      else
        lemma == s
      end
    }
    {any, any ? flemma : ""}
  end

  # def levelize_word_filters_old(filters)
  #  filters.reduce({} of String => String) { |memo, w|
  #    k = ASCIIFolder.replacing(w, "_")
  #    memo["#{k}"] = w
  #    memo
  #  }
  # end

  def levelize_word_filters(filters : Hash(String, Array(String)))
    filters.reduce({} of String => {String, String, Array(String)}) { |memo, w|
      cat, values = w
      values.each { |val|
        parts = val.split("@")
        lemma = parts.shift
        k = ASCIIFolder.replacing(lemma, "_")
        memo["#{k}"] = {cat, lemma, parts}
      }
      memo
    }
  end

  def export_norm_filtered(
    markers, outfilename, lemmas,
    filters : Hash(String, {String, String, Array(String)}),
    filter_compare = Types::CompareFlags::Default,
    splitted = true,
    dirpath : Nil | String = nil
  )
    seps = markers.separators
    res = lemmas.reduce({"", [] of {String, String, Array(String), String}}) { |memo, lemma|
      names = lemma[1].split(",").map(&.strip)

      lname = ASCIIFolder.replacing(names[0], "_")

      any, flemma = Utils.strings_includes(lname, filters.keys, filter_compare)

      category = if (!filters.empty? && any)
                   cat, lword, subcats = filters["#{flemma}"]
                   names[0] = lword
                   cat
                 else
                   ""
                 end

      fname = "#{outfilename}-#{lname}.txt"
      first = lemma[3][0].to_s
      mark = first == seps["see"] || first == seps["equal"] ? "" : ":"
      text = "#{names.join(", ")}#{mark} #{lemma[3]}.\n\n"
      memo = {
        memo[0] + text,
        memo[1] << {category, fname, subcats.nil? ? [] of String : subcats, text},
      }
      memo
    }

    save_source(
      "#{outfilename}#{FILTERED_SUFFIX}.txt", "\n\n#{res[0]}",
      dirpath) unless res[0].empty?

    res[1].each { |item|
      cat, lemma, subcats, text = item
      dpath =
        cat.empty? ? dirpath : dirpath.nil? ? category_as_path(cat) : File.join(dirpath, category_as_path(cat))
      save_source(lemma, "\n\n#{text}", dpath)

      unless subcats.empty?
        subcats.each { |subcat|
          dpath = dirpath.nil? ? category_as_path(subcat) : File.join(dirpath, category_as_path(subcat))
          save_source(lemma, "\n\n#{text}", dpath)
        }
      end
    } if splitted
  end

  def export_teilexes(outfilename,
                      teilexs,
                      t : Symbol = :xml,
                      docStruct : DocumentStruct? = nil)
    teilexes = Dictnorm1.filter_teilexs(teilexs).join
    source = Utils.xmlhead("<body>#{teilexes}</body>")
    ext = "xml"
    case t
    when :yaml
      ext = "xml.#{t.to_s}"
      source = XmlSerializer.xml_to_yaml(source)
    when :json
      ext = "xml.#{t.to_s}"
      source = XmlSerializer.xml_to_json(source)
    when :tei
      ext = "#{t.to_s}.xml"
      source = XmlSerializer.xml_tei_wrapped(source, docStruct)
    end
    Utils.save_source("#{outfilename}.#{ext}", source, ext.gsub(".", "-").upcase)
  end

  def export_splitted(
    outfilename : String,
    sources : Array(LemmaSource),
    suffix : String,
    msg : String,
    t : Symbol = :source,
    dirpath : Nil | String = nil,
    verbose = false,
    docStruct : DocumentStruct? = nil
  )
    op = ->(source : LemmaSource) {
      cat, lemma, subcats, text, ext =
        case t
        when :source
          src = source.as({String, String, Array(String), String})
          {src[0], src[1], src[2], src[3], "txt"}
        when :xml
          src = source.as({String, String, Array(String), String})
          {src[0], src[1], src[2], Utils.xmlhead_root(src[3]), "xml"}
        when :tei
          src = source.as({String, String, Array(String), String})
          content = XmlSerializer.xml_tei_wrapped(Utils.xmlhead_root(src[3]), docStruct)
          {src[0], src[1], src[2], content, "tei.xml"}
        when :xmlyaml
          src = source.as({String, String, Array(String), String})
          content = XmlSerializer.xml_to_yaml(Utils.xmlhead_root(src[3]))
          {src[0], src[1], src[2], content, "xml.yaml"}
        when :xmljson
          src = source.as({String, String, Array(String), String})
          content = XmlSerializer.xml_to_json(Utils.xmlhead_root(src[3]))
          {src[0], src[1], src[2], content, "xml.json"}
        when :json
          src = source.as(LemmaStruct)
          {src["category"], src["lemma"], src["subcategories"], src.to_json, "json"}
        when :yaml
          src = source.as(LemmaStruct)
          {src["category"], src["lemma"], src["subcategories"], src.to_yaml, "yml"}
        else
          src = source.as({String, String, Array(String), String})
          {src[0], src[1], src[2], src[3], "txt"}
        end
      lname = ASCIIFolder.replacing(lemma, "_")
      fname = "#{outfilename}#{suffix}-#{lname}.#{ext}"
      dpath =
        cat.empty? ? dirpath : dirpath.nil? ? category_as_path(cat) : File.join(dirpath, category_as_path(cat))
      Utils.print_info("Splitting #{t}: #{fname}", verbose)
      Utils.save_source(fname, text, dpath)
      unless subcats.empty?
        subcats.each { |subcat|
          dpath = dirpath.nil? ? category_as_path(subcat) : File.join(dirpath, category_as_path(subcat))
          save_source(fname, text, dpath)
        }
      end
    }
    sources.each(&op)
  end
end
