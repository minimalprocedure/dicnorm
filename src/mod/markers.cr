# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

require "yaml"
require "cuid"

include Types

module Markers
  extend self

  struct FiltersStruct
    include YAML::Serializable

    property lemmas

    def initialize(@lemmas : Hash(String, Array(String)))
    end
  end

  struct AuthorsData
    include YAML::Serializable

    property authors

    def initialize(
      @authors : Types::AuthorsData
    )
    end

    def add(author)
      unless @authors.has_key?(author)
        uuid = CUID.random.to_s
        autdata = {
          sname:   author,
          name:    "_",
          from:    "0",
          to:      "0",
          id:      uuid,
          refname: "_",
        }
        @authors[author] = autdata
        autdata
      else
        @authors[author]
      end
    end
  end

  struct MarkersStruct
    include YAML::Serializable

    property document
    property symbols
    property wraps
    property xmltei
    property separators
    property author_resp
    property authors
    property authors_data
    property authors_ref_prefix
    property suffix
    property prefix
    property subst
    property chars_subst
    property annots
    property grams
    property splitters
    property function

    def initialize(
      @document : Types::DocumentStruct,
      @symbols : Types::SymbolsStruct,
      @wraps : Types::WrapsStruct,
      @xmltei : Types::XmlTeiStruct,
      @separators : Types::SeparatorsStruct,
      @author_resp : Types::AuthorResp,
      @authors : Types::AuthorsStruct,
      @authors_data : AuthorsData?,
      @authors_ref_prefix : String,
      @suffix : Types::SuffixStruct,
      @prefix : Types::PrefixStruct,
      @subst : Types::SubstStruct,
      @chars_subst : Types::CharsSubstStruct,
      @annots : Types::AnnotsStruct,
      @grams : Types::GramsStruct,
      @splitters : Types::SplittersStruct,
      @function : Types::FunctionStruct
    )
    end

    def add_author_datum(author)
      ret = nil
      authors_data.try { |ref|
        ret = ref.add(author)
      }
      ret
    end
  end

  def normalize_authors(markers)
    authors = markers.authors.values.uniq
    authors.each { |a|
      markers.authors["#{a}"] = a
    }
    markers
  end

  def load(markers : String | Nil = nil)
    yamlfile =
      if markers
        markers
      else
        File.join(
          Path.new(Process.executable_path.to_s).dirname,
          "..", "conf/markers.yml"
        )
      end
    if File.exists?(yamlfile)
      begin
        markers = MarkersStruct.from_yaml(File.read(yamlfile))
        normalize_authors(markers)
      rescue ex
        Utils.exit_error("Markers malformed: #{yamlfile}:\n#{ex.message}")
      end
    else
      Utils.exit_error("Markers not exists: #{yamlfile}")
    end
  end

  def load_authors_data(authors_data_file : String | Nil = nil)
    yamlfile =
      if authors_data_file
        authors_data_file
      else
        File.join(
          Path.new(Process.executable_path.to_s).dirname,
          "..", "conf/authors-data.yml"
        )
      end
    if File.exists?(yamlfile)
      begin
        AuthorsData.from_yaml(File.read(yamlfile))
      rescue ex
        Utils.exit_error("Author Data malformed: #{yamlfile}:\n#{ex.message}")
      end
    else
      AuthorsData.new(Types::AuthorsData.new)
    end
  end

  def load_filters(filters : String | Nil = nil)
    yamlfile =
      if filters
        filters
      else
        File.join(
          Path.new(Process.executable_path.to_s).dirname,
          "..", "conf/filters.yml"
        )
      end
    if File.exists?(yamlfile)
      begin
        FiltersStruct.from_yaml(File.read(yamlfile))
      rescue ex
        Utils.exit_error("Filters malformed: #{yamlfile}:\n#{ex.message}")
      end
    else
      Utils.exit_error("Filters not exists: #{yamlfile}")
    end
  end

  def wrap_source(wraps, s)
    " #{wraps[0]}#{s}#{wraps[1]} "
  end

  def oring_by_sep(separators)
    separators.keys.map { |k|
      separators["#{k}"]
    }.join("|")
  end

  def regex_by_sep(separators)
    seps = oring_by_sep(separators)
    /(.+?)(?=#{seps}|$)/imx
  end

  def extract_sense_tag_attributes(source)
    # rex = /\[((\w+)\|*(\w*))+:([^\(\]]*)((?:\({0,1})([\w!'\#\%\&'\*\+,-\.\/:;\<=\>\?\@\^_{\|}~ ]*)(?:\){0,1}))\]/imx
    # rex = "\\[((\\w+)\\|*(\\w*))+:([^\\(\\]]*)((?:\\({0,1})([\\w!'\\#\\%\\&'\\*\\+,-\\.\\/:;\\<=\\>\\?\\@\\^_{\\|}~ ]*)(?:\\){0,1}))\\]"
    rex = "\\[((\\w+)\\|*(\\w*))+:([^\\{\\]]*)((?:\\{{0,1})([\\w!'\\#\\%\\&'\\*\\+,-\\.\\/:;\\<=\\>\\?\\@\\^_\\(\\|\\)~ ]*)(?:\\}{0,1}))\\]"
    rex = Regex.new(rex,
      options: Regex::CompileOptions::IGNORE_CASE | Regex::CompileOptions::MULTILINE | Regex::CompileOptions::EXTENDED # | #Regex::CompileOptions::UTF_8
    )
    begin
      source.scan(rex).reduce({source, [] of Array(String)}) { |data, m|
        text, blocks = data
        full = m[0]
        tag = m[2]
        attr = m[3]
        cont1 = m[4]
        cont2 = m[6]
        blocks << (if attr && attr.size > 0
          if cont2 && cont2.size > 0
            [tag.strip, attr.strip, cont1.strip, cont2.strip]
          else
            [tag.strip, attr.strip, cont1.strip]
          end
        else
          [tag.strip, cont1.strip]
        end)
        {text.gsub(full, ""), blocks}
      }
    rescue ex
      Utils.exit_error("Offending tag source: #{source}:\n-----\n#{ex.message}")
    end
  end

  def wrap_sense_custom_tag(xml, text)
    text, blocks = extract_sense_tag_attributes(text)
    if blocks.size > 0
      blocks.each { |block|
        if block.size == 4
          xml.element(block[0], Utils.attrs_h([[block[1], block[2]]])) {
            xml.text(block[3])
          }
        end
        if block.size == 3
          xml.element(block[0], Utils.attrs_h([[block[1], block[2]]]))
        end
        if block.size == 2
          xml.element(block[0]) {
            xml.text(block[1])
          }
        end
      }
    end
    {xml, text}
  end

  def wrap_definitions(xml, markers, text, refid = "")
    t = markers.xmltei
    xml, text = wrap_sense_custom_tag(xml, text)
    xml.element(t["cit_translation"]["tag"], Utils.attrs_h(t["cit_translation"]["attrs"])) {
      xml.element(t["form"]["tag"], Utils.attrs_h(t["form"]["attrs"])) {
        xml.element(t["orth"]["tag"]) {
          xml.text(text.strip)
        }
      }
    }
    xml
  end

  def wrap_definitions_with_ref(xml, markers, text, refid = "")
    t = markers.xmltei
    xml, text = wrap_sense_custom_tag(xml, text)
    xml.element(t["def_"]["tag"], Utils.attrs_h(t["def_"]["attrs"])) {
      # xml.text(text.strip)
      xml.element(t["xr"]["tag"], Utils.attrs_h(t["xr"]["attrs"])) {
        xml.element(t["ref"]["tag"],
          Utils.attrs_h(t["ref"]["attrs"] + [["target", refid]])) {
          xml.text(text.strip)
        }
      }
    }
    xml
  end

  def get_tresp(tresp, author_datum)
    resp = tresp
    author_datum.try { |author_datum|
      if author_datum["refname"] != "_" || author_datum["refname"].empty?
        resp = author_datum["refname"]
      end
    }
    resp
  end

  def wrap_author(xml, author_datum)
    xml.element("author", {"ref": author_datum["id"]}) {
      xml.element("persName", {"type": "usualname"}) {
        xml.text(author_datum["name"])
      }
      xml.element("persName", {"type": "abbrname"}) {
        xml.text(author_datum["sname"])
      }
      xml.element("date", {"from": author_datum["from"], "to": author_datum["to"]})
    }
  end

  def wrap_pgram_author(xml, markers, aquote, author, tquote, tresp)
    author_datum = markers.add_author_datum(author)
    tresp = get_tresp(tresp, author_datum)
    strip_chars = /[=]/
    t = markers.xmltei
    xml.element(t[:form]["tag"],
      Utils.attrs_h(t[:form]["attrs"] + [["type", "variant"]])
    ) {

      xml.element(t[:orth]["tag"],
        Utils.attrs_h(t[:orth]["attrs"])) {
        xml.text(aquote.gsub(strip_chars, "").strip)
      } unless aquote.empty? || aquote == "NULL"

      xml.element(t[:orth]["tag"],
        Utils.attrs_h(t[:orth]["attrs"])) {
        xml.text(tquote.gsub(strip_chars, "").strip)
      } unless tquote.empty? || tquote == "NULL"

      xml.element(t[:bibl]["tag"], Utils.attrs_h(t[:bibl]["attrs"])) {
        author_datum.try { | author_datum |
          # xml.element(t[:author]["tag"], Utils.attrs_h(t[:author]["attrs"] + [["ref", author_datum["id"]]])) {
          #  xml.text(author_datum["name"])
          # }
          wrap_author(xml, author_datum)
        }
      } unless author.empty?
    }
    xml
  end

  def wrap_cit_tags(
    xml, markers, aquote, author, tquote, tresp,
    title = "", publisher = "", date = "", bibl_scope = ""
  )
    author_datum = markers.add_author_datum(author)
    tresp = get_tresp(tresp, author_datum)

    t = markers.xmltei
    xml.element(t[:cit]["tag"], Utils.attrs_h(t[:cit]["attrs"] + [["resp", tresp]])) {
      unless (tquote == "NULL")
        xml.element(t[:aquote]["tag"], Utils.attrs_h(t[:aquote]["attrs"])) {
          xml.text(aquote)
        }
        xml.element(t[:tquote]["tag"], Utils.attrs_h(t[:tquote]["attrs"])) {
          xml.text(tquote)
        }
      else
        xml.element(t["cit_translation"]["tag"], Utils.attrs_h(t["cit_translation"]["attrs"])) {
          xml.element(t[:tquote]["tag"], Utils.attrs_h(t[:tquote]["attrs"])) {
            xml.text(aquote)
          }
        }
      end
      xml.element(t[:bibl]["tag"], Utils.attrs_h(t[:bibl]["attrs"])) {
        author_datum.try { |author_datum|
          # xml.element(t[:author]["tag"], Utils.attrs_h(t[:author]["attrs"] + [["ref", author_datum["id"]]])) {
          #  xml.text(author_datum["name"])
          # }
          wrap_author(xml, author_datum)
        }
        xml.element(t[:title]["tag"], Utils.attrs_h(t[:title]["attrs"])) {
          xml.text(title)
        }
        xml.element(t[:publisher]["tag"], Utils.attrs_h(t[:publisher]["attrs"])) {
          xml.text(publisher)
        }
        xml.element(t[:date]["tag"], Utils.attrs_h(t[:date]["attrs"])) {
          xml.text(date)
        }
        xml.element(t[:biblScope]["tag"], Utils.attrs_h(t[:biblScope]["attrs"])) {
          xml.text(bibl_scope)
        }
      }
    }
    xml
  end

  def wrap_cit(markers, acit, auth, tcit, tresp)
    symbols = markers.symbols
    [
      " ",
      symbols[:cit],
      " ",
      acit,
      " ",
      auth,
      ":",
      tresp,
      " ",
      tcit,
      " ",
      symbols[:cit],
      " ",
    ].join
  end

  private def quote_or_def(a, b, c, d, defa = "", defb = "", defc = "", defd = "")
    {
      a ? a.as(String) : defa,
      b ? b.as(String) : defb,
      c ? c.as(String) : defc,
      d ? d.as(String) : defd,
    }
  end

  private def match_author(author, wraps, text, markers)
    o = wraps[0]
    c = wraps[1]
    r = /\s*(.*)\s+#{"\\" + o}(#{author})#{"\\" + c}\s+(.*)\s*/imx
    m = text.match(r)
    if m.nil?
      "#{text} NULL".match(r)
    else
      m
    end
  end

  def author_resp(author, author_resp = "")
    ar = author.nil? ? ["", ""] : author.split(":")
    ar.size > 1 ? {ar[0], ar[1]} : {ar[0], author_resp}
  end

  def split_text_by_authors(texts, authors, markers)
    wraps = markers.wraps["author"]
    author_resp = markers.author_resp
    o = wraps[0]
    c = wraps[1]
    texts.reduce({[] of String, [] of {String, String, String, String}}) { |memo, text|
      finded = authors.find { |author|
        !(text.index("#{o}#{author}#{c}").nil?)
      }
      if finded.nil?
        memo[0] << text
      else
        matches = match_author(finded, wraps, text, markers)
        if !matches.nil?
          author, resp = author_resp(matches.captures[1], author_resp)
          aquote, author, tquote, tresp = quote_or_def(
            matches.captures[0],
            author,
            matches.captures[2],
            resp
          )
          memo[1] << {aquote, author, tquote, tresp}
        end
      end
      memo
    }
  end

  def description_to_xml(markers, xml, source, _type, data = {} of String => String)
    authors = markers.authors
    splitters = markers.splitters
    t = markers.xmltei
    if source.nil?
      xml
    else
      anames = authors.values.uniq
      texts = source.split(";").map(&.strip)
      raws, structs = split_text_by_authors(texts, anames, markers)
      xml =
        structs.reduce(xml) { |xmemo, _struct|
          aquote, author, tquote, tresp = _struct
          xmemo =
            case _type
            when :pgr
              wrap_pgram_author(xmemo, markers, aquote, author, tquote, tresp)
            else
              wrap_cit_tags(xmemo, markers, aquote, author, tquote, tresp)
            end
        }
      xml =
        raws.reduce(xml) { |xmemo, raw|
          xmemo =
            case _type
            when :pgr
              parts = raw.split("=").map { |s| s.strip }
              last = parts.size < 2 ? "" : parts[1]
              wrap_pgram_author(xmemo, markers, parts[0], "", last, "")
            when :equ
              wrap_definitions_with_ref(xmemo, markers, raw, data[raw])
            when :see
              wrap_definitions_with_ref(xmemo, markers, raw, data[raw])
            else
              parts = raw.split(splitters["defs"]).map { |s| s.strip }
              parts.reduce(xml) { |xmemo, raw|
                wrap_definitions(xmemo, markers, raw)
              }
            end
        }
      unless texts.empty?
        xml.element(t["note"]["tag"],
          Utils.attrs_h(t["note"]["attrs"] + [["motivation", "editing"]])) {
          texts.each { |text|
            xml.element(t["span"]["tag"], Utils.attrs_h(t["span"]["attrs"])) {
              xml.text(text)
            }
          }
        }
      end
      xml
    end
  end

  def wrap_citation(markers, source)
    authors = markers.authors
    author_resp = markers.author_resp
    wrap = ->(memo : String, k : String) {
      matches = match_author(authors["#{k}"], markers.wraps["author"], memo, markers)
      if matches.nil?
        memo
      else
        author, resp = author_resp(matches.captures[1], author_resp)
        aquote, author, tquote, tresp = quote_or_def(
          matches.captures[0],
          author,
          matches.captures[2],
          resp
        )
        wrap_cit(markers, aquote, author, tquote, tresp)
      end
    }
    if source.nil?
      ""
    else
      parts = source.split(";")
      (parts.map { |part|
        authors.keys.reduce(part, &wrap).squeeze(" ").strip
      }).join("; ")
    end
  end
end
