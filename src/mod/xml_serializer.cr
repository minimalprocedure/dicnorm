# TODO: Write documentation for `Xml2hash`

require "xml"
require "yaml"
require "json"
require "./lib_xml_ext"

module XmlSerializer
  extend self

  struct TDocStruct
    include YAML::Serializable
    include JSON::Serializable

    property name
    property text
    property attributes
    property children

    def initialize(
      @name : String? = nil,
      @text : String? = nil,
      @attributes : Hash(String, String)? = nil,
      @children : Array(TDocStruct | Hash(String, String))? = nil
    )
    end

    def attributes_from_node(node)
      if @attributes.nil? && node.attributes.size > 0
        @attributes = {} of String => String
      end
      node.attributes.reduce({} of String => String) { |memo, attr|
        @attributes.try { |prop| prop[attr.name] = attr.content }
      }
    end

    def name_from_node(node)
      @name = node.name unless node.text?
      if node.text? && !node.text.strip.empty?
        @text = node.text.strip
      end
    end

    def empty_children(node)
      if @children.nil? && node.children.size > 0
        @children = [] of TDocStruct | Hash(String, String)
      end
    end

    def process_children(node, parent)
      node.children.each { |subnode|
        if subnode.text? && !subnode.text.strip.empty?
          child = TDocStruct.new
          child.text = subnode.text
          parent.children.try { |prop| prop << child }
        end
        if !subnode.text?
          child = TDocStruct.new
          child.name_from_node(subnode)
          child.attributes_from_node(subnode)
          child.empty_children(subnode)
          parent.children.try { |prop| prop << child }
          if subnode.children.size > 0
            subchild = TDocStruct.new
            subchild.empty_children(subnode)
            subchild.process_children(subnode, child)
          end
        end
      }
    end

    def from_node(node)
      name_from_node(node)
      attributes_from_node(node)
      empty_children(node)
      process_children(node, self)
    end

    def xml_to_json(xml)
      from_node(xml)
      to_json
    end

    def xml_to_yaml(xml)
      from_node(xml)
      to_yaml
    end
  end

  def xml_tei_wrapped(xmldoc : XML::Node | String,
                      docStruct : DocumentStruct? = nil)
    title, publisher = if docStruct
                         {docStruct[:title], docStruct[:publisher]}
                       else
                         {"TITLE", "PUBLISHER"}
                       end
    XML.build(indent: "  ", encoding: "UTF-8") do |xml|
      xml.element("TEI",
        {"xmlns" => "http://www.tei-c.org/ns/1.0",
         "type"  => "lexicon"}) do
        xml.element("teiHeader") do
          xml.element("fileDesc") do
            xml.element("titleStmt") do
              xml.element("title") do
                xml.text(title)
              end
            end
            xml.element("publicationStmt") do
              xml.element("publisher") do
                xml.text(publisher)
              end
            end
          end
        end
        xml.element("text") do
          entries = xmldoc.is_a?(String) ? XML.parse(xmldoc) : xmldoc
          xml.text_raw(entries.to_xml(
            options: XML::SaveOptions.new(
              XML::SaveOptions::NO_DECL |
              XML::SaveOptions::FORMAT |
              XML::SaveOptions::AS_XML
            )))
        end
      end
    end
  end

  def xml_to_json(xmldoc : XML::Node | String)
    xml = xmldoc.is_a?(String) ? XML.parse(xmldoc) : xmldoc
    ts = TDocStruct.new
    ts.from_node(xml)
    ts.to_json
  end

  def xml_to_yaml(xmldoc : XML::Node | String)
    xml = xmldoc.is_a?(String) ? XML.parse(xmldoc) : xmldoc
    ts = TDocStruct.new
    ts.from_node(xml)
    ts.to_yaml
  end
end
