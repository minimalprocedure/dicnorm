module Version
  extend self
  MAJOR = "0"
  MINOR = "9"

  macro finished
    BUILD = {{ `date +%Y%m%d%H%M%S`.stringify }}

    def get
      "#{Version::MAJOR}.#{Version::MINOR}.#{Version::BUILD}"
    end
  end
end
