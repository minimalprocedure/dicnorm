require "option_parser"
require "json"
require "yaml"
require "./mod/dicnorm_1.cr"
require "./mod/utils.cr"
require "./mod/version.cr"

DEF_TAG       = "norm"
NORM_TAG      = "norm"
FILTERED_TAG  = "FILTERED"
REBUILDED_TAG = "REBUILDED"
DEF_SUFFIX    = "-#{DEF_TAG}"
NORM_SUFFIX   = "-#{NORM_TAG}"
# FILTERED_SUFFIX  = "-#{FILTERED_TAG}"
# REBUILDED_SUFFIX = "-#{REBUILDED_TAG}"

FILTERED_SUFFIX  = ""
REBUILDED_SUFFIX = ""

infilename = ""
outfilename = ""
jsonfile = false
yamlfile = false
teilexfile = false
teilex_jsonfile = false
teilex_yamlfile = false
export_filtered = false
rebuildfile = false
markers_file = nil
filters_file = nil
splitting = false
verbose = false
norm_phases = false
print_version = false
tree_output = false
filter_compare = Types::CompareFlags::Default

OptionParser.parse do |parser|
  parser.banner = "Dictnorm v.: #{Version.get}\nUsage: dictnorm [arguments]"
  parser.on("-i FILENAME", "--in=Filename", "input dict txt file") { |fn| infilename = fn }
  parser.on("-o FILENAME", "--out=Filename no ext", "output normalized dict txt file") { |fn| outfilename = fn }
  parser.on("-m FILENAME", "--markers=Filename", "markers yamlfile") { |fn| markers_file = fn }
  parser.on("-f FILENAME", "--filters=Filename", "filters yamlfile") { |fn| filters_file = fn }
  parser.on("-c [0|1|2]", "--compare=[0|1|2]",
    "filter compare flags") { |n| filter_compare = Types::CompareFlags.new(n.to_i) }
  parser.on("-r", "--rebuild", "output rebuilded text structure") { rebuildfile = true }
  parser.on("-e", "--export_filtered", "output filtered text") { export_filtered = true }
  parser.on("-j", "--json", "output json structure") { jsonfile = true }
  parser.on("-y", "--yaml", "output yaml structure") { yamlfile = true }
  parser.on("-x", "--xml", "output xml (TEILex) structure") { teilexfile = true }
  parser.on("--xmlyaml", "output xml (TEILex) structure as YAML") {
    teilexfile = true
    teilex_yamlfile = true
  }
  parser.on("--xmljson", "output xml (TEILex) structure as JSON") {
    teilexfile = true
    teilex_jsonfile = true
  }
  parser.on("-s", "--split", "split lemmas in own file") { splitting = true }
  parser.on("-d", "--describe", "verbose ops") { verbose = true }
  parser.on("-p", "--phases", "output all normalization") { norm_phases = true }
  parser.on("-t", "--tree", "tree output") { tree_output = true }
  parser.on("-v", "--version", "print version") { print_version = true }
  parser.on("-h", "--help", "Show this help") do
    puts parser
    exit
  end
  parser.invalid_option do |flag|
    STDERR.puts "ERROR: #{flag} is not a valid option."
    STDERR.puts parser
    exit(1)
  end
end

if print_version
  puts "Dictnorm v.: #{Version.get}"
end

if infilename.empty? || !File.exists?(infilename)
  STDERR.puts "ERROR: please set infile."
  exit
end

markers =
  if markers_file.nil?
    Markers.load
  else
    Markers.load(markers_file)
  end

filters =
  if filters_file.nil?
    nil
  else
    Markers.load_filters(filters_file)
  end

def run!(
  infilename,
  outfilename,
  rebuildfile,
  jsonfile,
  yamlfile,
  teilexfile,
  teilex_yamlfile,
  teilex_jsonfile,
  export_filtered,
  splitting,
  verbose,
  norm_phases,
  markers,
  filters,
  filter_compare,
  tree_output,
  markers_file,
  filters_file
)
  outfile = !outfilename.empty?
  unless outfile
    path = Path[infilename]
    outfilename = Path[path.dirname]
      .join("#{path.stem}")
      .to_s
  end

  author_data_file = File.join(
    markers_file.nil? ? FileUtils.pwd : File.dirname(markers_file),
    "authors-data.yml")

  Markers.load_authors_data(author_data_file).try { |authors_data|
    markers.authors_data = authors_data
    authors_data.authors.each_key { |k| markers.authors[k] = k }
  }

  Utils.print_info("============================================================", verbose)
  Utils.print_info("Open source: #{infilename}", verbose)
  Utils.print_info("------------------------------------------------------------", verbose)
  insource = Utils.open_source(infilename)

  words_filters = filters.nil? ? Hash(String, {String, String, Array(String)}).new : Utils.levelize_word_filters(filters.lemmas)

  source =
    if infilename.match(/[^\\]*(#{NORM_SUFFIX})\.(\w+)$/)
      s = Dictnorm1.normalize_phases(markers, insource, {"words"})
      s = Dictnorm1.normadapt_text(s)
      Utils.save_source("#{outfilename}.txt", s) if outfile
      s
    else
      Utils.print_info("Normalize phase 1...", verbose)
      s = Dictnorm1.normalize_phases(markers, insource, {"text", "chars", "words"})
      Utils.save_source("#{outfilename}#{NORM_SUFFIX}-p1.txt", s) if norm_phases
      Utils.print_info("Normalize phase 2...", verbose)
      s = Dictnorm1.normprocess_text(markers, s)
      Utils.save_source("#{outfilename}#{NORM_SUFFIX}-p2.txt", s) if norm_phases
      Utils.print_info("Normalize phase 3...", verbose)
      s = Dictnorm1.normadapt_text(s)
      s = Dictnorm1.normadapt_lemmas(s, words_filters, filter_compare)
      s = Dictnorm1.push_left_lines(s)
      Utils.save_source("#{outfilename}#{NORM_SUFFIX}-p3.txt", s) if norm_phases
      Utils.save_source("#{outfilename}#{NORM_SUFFIX}.txt", s)
      s
    end

  if rebuildfile || teilexfile || jsonfile || yamlfile || export_filtered
    # words_filters = filters.nil? ? Hash(String, {String, String, Array(String)}).new : Utils.levelize_word_filters(filters.lemmas)

    Utils.print_info("Reduce lemmas chain...", verbose)
    chain = Dictnorm1.reduce_lemmas_chain(source, infilename, words_filters, filter_compare)
    Utils.print_info("Reduce lemmas from chain...", verbose)
    lemmas = Dictnorm1.reduce_lemmas(markers, chain, source)

    Utils.export_norm_filtered(
      markers, outfilename, lemmas, words_filters, filter_compare, splitting, FILTERED_TAG) if export_filtered

    Utils.print_info("Structurize...", verbose)
    structure = Dictnorm1.structurize(markers, lemmas)

    if rebuildfile
      Utils.print_info("Process texts...", verbose)
      sources = Dictnorm1.structurize_sources(markers, lemmas)
      Utils.save_source("#{outfilename}#{REBUILDED_SUFFIX}.txt",
        Dictnorm1.filter_sources(sources).join, REBUILDED_TAG)
      Utils.export_splitted(outfilename, sources, REBUILDED_SUFFIX, "Splitting texts: ", :source,
        tree_output ? REBUILDED_TAG : nil, verbose) if splitting
    end

    if teilexfile && !structure.empty?
      Utils.print_info("Process xml...", verbose)
      teilexs = Dictnorm1.structurize_teilexs(markers, structure)
      docStruct = markers.document
      Utils.export_teilexes(outfilename, teilexs, :xml)
      Utils.export_teilexes(outfilename, teilexs, :tei, docStruct)
      Utils.export_teilexes(outfilename, teilexs, :yaml) if teilex_yamlfile
      Utils.export_teilexes(outfilename, teilexs, :json) if teilex_jsonfile
      if splitting
        Utils.export_splitted(outfilename, teilexs, "", "Splitting xml: ", :xml,
          tree_output ? "XML" : nil, verbose)
        Utils.export_splitted(outfilename, teilexs, "", "Splitting tei: ", :tei,
          tree_output ? "TEI" : nil, verbose, docStruct)
        Utils.export_splitted(outfilename, teilexs, "", "Splitting xmlyaml: ", :xmlyaml,
          tree_output ? "XML-YAML" : nil, verbose) if teilex_yamlfile
        Utils.export_splitted(outfilename, teilexs, "", "Splitting xmljson: ", :xmljson,
          tree_output ? "XML-JSON" : nil, verbose) if teilex_jsonfile
      end
    end

    if jsonfile && !structure.empty?
      Utils.print_info("Process json...", verbose)
      Utils.save_source("#{outfilename}.json", structure.to_json, "json")
      Utils.export_splitted(
        outfilename, structure, "",
        "Splitting json: ", :json,
        tree_output ? "json" : nil,
        verbose) if splitting
    end

    if yamlfile && !structure.empty?
      Utils.print_info("Process yaml...", verbose)
      Utils.save_source("#{outfilename}.yml", structure.to_yaml, "yaml")
      Utils.export_splitted(
        outfilename, structure, "",
        "Splitting json: ", :yaml,
        tree_output ? "yaml" : nil,
        verbose) if splitting
    end
  end
  Utils.print_info("============================================================", verbose)
  Utils.print_info("Dump Autors Refs...", verbose)
  Utils.save_source(author_data_file, markers.authors_data.to_yaml, "")
  Utils.print_info("============================================================", verbose)
end

run!(
  infilename,
  outfilename,
  rebuildfile,
  jsonfile,
  yamlfile,
  teilexfile,
  teilex_yamlfile,
  teilex_jsonfile,
  export_filtered,
  splitting,
  verbose,
  norm_phases,
  markers,
  filters,
  filter_compare,
  tree_output,
  markers_file,
  filters_file
)
