# dicnorm

_Naive_ normalization and XML/JSON/YAML export for Tesseract dict output.

```
Dictnorm v.: 0.9.20240411120227
Usage: dictnorm [arguments]
    -i FILENAME, --in=Filename       input dict txt file
    -o FILENAME, --out=Filename no ext
                                     output normalized dict txt file
    -m FILENAME, --markers=Filename  markers yamlfile
    -f FILENAME, --filters=Filename  filters yamlfile
    -c [0|1|2], --compare=[0|1|2]    filter compare flags
    -r, --rebuild                    output rebuilded text structure
    -e, --export_filtered            output filtered text
    -j, --json                       output json structure
    -y, --yaml                       output yaml structure
    -x, --xml                        output xml (TEILex) structure
    --xmlyaml                        output xml (TEILex) structure as YAML
    --xmljson                        output xml (TEILex) structure as JSON
    -s, --split                      split lemmas in own file
    -d, --describe                   verbose ops
    -p, --phases                     output all normalization
    -t, --tree                       tree output
    -v, --version                    print version
    -h, --help                       Show this help
                    Show this help
```
