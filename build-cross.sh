#!/bin/bash

SOURCE=src/main.cr
OUT=/mnt/data/nissl/Experiments/TESTING5/cosmopolitan/cosmopolitan/dictnorm

./gen_version.sh

crystal build $SOURCE -o $OUT \
--static \
--cross-compile \
--target "x86_64-pc-linux-gnu" \
--release \
--verbose \
--link-flags "-flto"