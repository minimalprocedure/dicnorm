#!/bin/bash
shopt -s nullglob

BASE=$1
LETTER=$2
FRONT=$3

OCR=$LETTER-ocr
PROC=$LETTER-proc
CRS=$LETTER-crs
NORM=$LETTER-norm
CURR=$(realpath $(dirname $0))
SUFFIX="output-latin.txt"

cd $BASE

mkdir -p $OCR
mkdir -p $PROC
mkdir -p $CRS
mkdir -p $NORM

echo "--------------------------------------------------------------------------"
echo RESAMPLE $LETTER TO $BASE/$CRS WITH FRONT $FRONT
echo "--------------------------------------------------------------------------"
$CURR/resamples.sh ./$LETTER ./$CRS $FRONT

echo "--------------------------------------------------------------------------"
echo OCR $BASE/$CRS TO $BASE/$OCR
echo "--------------------------------------------------------------------------"

cd $CRS
PNGS=(*.png)

if [ ${#PNGS[@]} -ne 0 ]
then
	for f in ${PNGS[@]}
	do 
		b="${f%.*}" 		
		if [ ! -f "../$OCR/$b-$SUFFIX" ]; then
		  echo "RUN OCR FOR: $f"  
		  $CURR/ocrgo.sh "$b" "../$OCR" "$CURR/tessdata"
		else
		  echo "RUNNED OCR FOR: $f"
		fi
	done
else
	echo "OCR: EMPTY $BASE/$CRS"
fi

cd ..

echo "--------------------------------------------------------------------------"
echo DICNORM $BASE/$OCR TO $BASE/$PROC
echo "--------------------------------------------------------------------------"
cd $OCR
TXTS=(*-$SUFFIX)

if [ ${#TXTS[@]} -ne 0 ]
then
	for f in ${TXTS[@]}
	do
		b="${f%.*}" 
		echo "PROCESS: $b.txt -> $BASE/$PROC/$b"
		$CURR/dictnorm \
		-i $b.txt \
		-o "../$PROC/$b" \
		-m "$CURR/markers.yml" \
		-f "../filters-$LETTER.yml" \
    -c 1 \
    -r \
    --tree \
    --yaml \
		--xml \
    --xmlyaml \
    --xmljson \
		--split \
		--describe \
		--export_filtered
	done
else
	echo "DICNORM: EMPTY $BASE/$OCR"
fi

cd ..
