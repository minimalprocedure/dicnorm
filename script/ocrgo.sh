#!/bin/bash
INPUT=$1
OUTPUT=$2
TESSDATA=$3

mkdir -p $OUTPUT

echo "OCR: $INPUT.png IN: $OUTPUT TESSDATA: $TESSDATA"

tesseract \
$INPUT.png \
$OUTPUT/$INPUT-output-latin \
-c classify_enable_learning=0 \
--oem 3 \
--tessdata-dir $TESSDATA \
--psm 3 \
-l latincustom+Latin
