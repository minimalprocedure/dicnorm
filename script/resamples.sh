#!/bin/bash
shopt -s nullglob

INPUT=$1
OUTPUT=$2
FRONT=$3

mkdir -p $OUTPUT

cd $INPUT
PNGS=(*.png)

if [ ${#PNGS[@]} -ne 0 ]
then
	for f in ${PNGS[@]}
	do
		basename="${f%.*}" 
		if [ "$basename" -eq $FRONT ]
		then
			delta=360
		else
			delta=144
		fi
		if [ ! -f "../$OUTPUT/$basename.png" ]; then	
		  echo "CROPPING, RESAMPLING 2X: $basename"
		  magick $basename.png \
		  -crop 1455x2006+0+$delta \
		  -trim \
		  -filter Catrom \
		  -resize 200% \
		  -sharpen 0x1.0 \
		  "../$OUTPUT/$basename.png"
		else
		  echo "CROPPED, RESAMPLED 2X: $basename"
		fi
	done
else
	echo "RESAMPLE: EMPTY $INPUT"
fi

cd ..
