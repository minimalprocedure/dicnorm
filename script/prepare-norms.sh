#!/bin/bash
mkdir -p latin-letter-a-norm/A-norm
cp ./norms/A-output-latin-norm.txt latin-letter-a-norm/A-norm/A-output-latin-norm.txt
cp ./latin-letter-a/filters-A.yml latin-letter-a-norm/filters-A.yml

mkdir -p latin-letter-b-norm/B-norm
cp ./norms/B-output-latin-norm.txt latin-letter-b-norm/B-norm/B-output-latin-norm.txt
cp ./latin-letter-b/filters-B.yml latin-letter-b-norm/filters-B.yml 

mkdir -p latin-letter-c-norm/C-norm
cp ./norms/C-output-latin-norm.txt latin-letter-c-norm/C-norm/C-output-latin-norm.txt
cp ./latin-letter-c/filters-C.yml latin-letter-c-norm/filters-C.yml

mkdir -p latin-letter-d-norm/D-norm
cp ./norms/D-output-latin-norm.txt latin-letter-d-norm/D-norm/D-output-latin-norm.txt
cp ./latin-letter-d/filters-D.yml latin-letter-d-norm/filters-D.yml

mkdir -p latin-letter-e-norm/E-norm
cp ./norms/E-output-latin-norm.txt latin-letter-e-norm/E-norm/E-output-latin-norm.txt
cp ./latin-letter-e/filters-E.yml latin-letter-e-norm/filters-E.yml

mkdir -p latin-letter-f-norm/F-norm
cp ./norms/F-output-latin-norm.txt latin-letter-f-norm/F-norm/F-output-latin-norm.txt
cp ./latin-letter-f/filters-F.yml latin-letter-f-norm/filters-F.yml

mkdir -p latin-letter-g-norm/G-norm
cp ./norms/G-output-latin-norm.txt latin-letter-g-norm/G-norm/G-output-latin-norm.txt
cp ./latin-letter-g/filters-G.yml latin-letter-g-norm/filters-G.yml

mkdir -p latin-letter-h-norm/H-norm
cp ./norms/H-output-latin-norm.txt latin-letter-h-norm/H-norm/H-output-latin-norm.txt
cp ./latin-letter-h/filters-H.yml latin-letter-h-norm/filters-H.yml 

mkdir -p latin-letter-i-norm/I-norm
cp ./norms/I-output-latin-norm.txt latin-letter-i-norm/I-norm/I-output-latin-norm.txt
cp ./latin-letter-i/filters-I.yml latin-letter-i-norm/filters-I.yml

mkdir -p latin-letter-k-norm/K-norm
#cp ./norms/K-output-latin-norm.txt latin-letter-k-norm/K-norm/K-output-latin-norm.txt
#cp ./latin-letter-k/filters-K.yml latin-letter-k-norm/filters-K.yml

mkdir -p latin-letter-l-norm/L-norm
cp ./norms/L-output-latin-norm.txt latin-letter-l-norm/L-norm/L-output-latin-norm.txt
cp ./latin-letter-l/filters-L.yml latin-letter-l-norm/filters-L.yml

mkdir -p latin-letter-m-norm/M-norm
cp ./norms/M-output-latin-norm.txt latin-letter-m-norm/M-norm/M-output-latin-norm.txt
cp ./latin-letter-m/filters-M.yml latin-letter-m-norm/filters-M.yml

mkdir -p latin-letter-n-norm/N-norm
cp ./norms/N-output-latin-norm.txt latin-letter-n-norm/N-norm/N-output-latin-norm.txt
cp ./latin-letter-n/filters-N.yml latin-letter-n-norm/filters-N.yml 

mkdir -p latin-letter-o-norm/O-norm
cp ./norms/O-output-latin-norm.txt latin-letter-o-norm/O-norm/O-output-latin-norm.txt
cp ./latin-letter-o/filters-O.yml latin-letter-o-norm/filters-O.yml

mkdir -p latin-letter-p-norm/P-norm
cp ./norms/P-output-latin-norm.txt latin-letter-p-norm/P-norm/P-output-latin-norm.txt
cp ./latin-letter-p/filters-P.yml latin-letter-p-norm/filters-P.yml

mkdir -p latin-letter-q-norm/Q-norm
cp ./norms/Q-output-latin-norm.txt latin-letter-q-norm/Q-norm/Q-output-latin-norm.txt
cp ./latin-letter-q/filters-Q.yml latin-letter-q-norm/filters-Q.yml

mkdir -p latin-letter-r-norm/R-norm
cp ./norms/R-output-latin-norm.txt latin-letter-r-norm/R-norm/R-output-latin-norm.txt
cp ./latin-letter-r/filters-R.yml latin-letter-r-norm/filters-R.yml

mkdir -p latin-letter-s-norm/S-norm
cp ./norms/S-output-latin-norm.txt latin-letter-s-norm/S-norm/S-output-latin-norm.txt
cp ./latin-letter-s/filters-S.yml latin-letter-s-norm/filters-S.yml

mkdir -p latin-letter-t-norm/T-norm
cp ./norms/T-output-latin-norm.txt latin-letter-t-norm/T-norm/T-output-latin-norm.txt
cp ./latin-letter-t/filters-T.yml latin-letter-t-norm/filters-T.yml

mkdir -p latin-letter-u-norm/U-norm
cp ./norms/U-output-latin-norm.txt latin-letter-u-norm/U-norm/U-output-latin-norm.txt
cp ./latin-letter-u/filters-U.yml latin-letter-u-norm/filters-U.yml

mkdir -p latin-letter-v-norm/V-norm
cp ./norms/V-output-latin-norm.txt latin-letter-v-norm/V-norm/V-output-latin-norm.txt
cp ./latin-letter-v/filters-V.yml latin-letter-v-norm/filters-V.yml

mkdir -p latin-letter-x-norm/X-norm
cp ./norms/X-output-latin-norm.txt latin-letter-x-norm/X-norm/X-output-latin-norm.txt
cp ./latin-letter-x/filters-X.yml latin-letter-x-norm/filters-X.yml

mkdir -p latin-letter-z-norm/Z-norm
cp ./norms/Z-output-latin-norm.txt latin-letter-z-norm/Z-norm/Z-output-latin-norm.txt
cp ./latin-letter-z/filters-Z.yml latin-letter-z-norm/filters-Z.yml

