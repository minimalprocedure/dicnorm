#!/bin/bash
shopt -s nullglob

BASE=$1
LETTER=$2

NORM=$LETTER-norm
PROC=$LETTER-proc-norm
CURR=$(realpath $(dirname $0))
SUFFIX="output-latin-norm.txt"

cd $BASE

mkdir -p $NORM
mkdir -p $PROC

echo "--------------------------------------------------------------------------"
echo DICNORM $BASE/$NORM TO $BASE/$PROC
echo "--------------------------------------------------------------------------"
cd $NORM
TXTS=(*-$SUFFIX)

if [ ${#TXTS[@]} -ne 0 ]
then
	for f in ${TXTS[@]}
	do
		b="${f%.*}" 
		echo "PROCESS: $b.txt -> $BASE/$PROC/$b"
		$CURR/dictnorm \
		-i $b.txt \
		-o "../$PROC/$b" \
		-m "$CURR/markers.yml" \
		-f "../filters-$LETTER.yml" \
    -c 1 \
    -r \
    --tree \
    --yaml \
		--xml \
    --xmlyaml \
    --xmljson \
		--split \
		--describe \
		--export_filtered
	done
else
	echo "DICNORM: EMPTY $BASE/$OCR"
fi

cd ..
