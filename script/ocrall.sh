#!/bin/bash
INPUT=$1
OUTPUT=$2
TESSDATA=$3
mkdir -p $OUTPUT

cd $INPUT
PNGS=*.png

for f in $PNGS
do 
	b="${f%.*}" 
	echo "RUN OCR FOR: $INPUT/$f"  
	../../ocrgo.sh "$b" "$OUTPUT" ./tessdata
done

cd ..

