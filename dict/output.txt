avanzo/avvedutezza

avanzata, aetas ingravescens Cc; sentinella avanzata,
prima statio, primae statiōnis, f.; a notte avanzata, muliā
nocte Cc; essere di idee avanzate, novis rebus studēre
© residuo, residūus, a, um.
avanzo: reliqušae, ārum, f.pl.; reľquum, i, n.; gli
avanzi, i ruderi, ružnae, ārum, f. pl.
avaramente: avāre; avidamente, cupšde.
avaria: guasto, detrimentum, i, n.; nave in avaria, na-
viS afflicta CEs.
avariare: guastare, corrumpo, is, corrūņi, corruptum,
corrumpěre, tr. 3.
avariato, agg.: guasto, corruptus, a, um e (detto di
navi), afflictus, a, um.
avariżia: avaria, ae, f.
avaro: avarus, a, um.
ave: ave.
Avella: Abella, ae, f.; di Avella, Abellānus a, um; abi-
tanti di Avella, Abellāni, ōrum, m. pl.
avellana: nocciola, (nux) Abellāna, (nucis) Abellānae,
f. o.
Avellino: Abellinum, i, n.; di Avellino, Abellīnas,
Abellinātis, m.; abitanti di Avellino, Abellinātes, tum,
m. pl.
avello: sepulcrum, i i, n
avena: avēna, ae, f.
Aventino: Aventinum, i, n.; dell’ Aventino, Aventīnus,
a, UM.
1. avere, verbo: quando è ausiliare non si traduce:
ho fatto, feci © possedere, tenere: a) (detto di cose
© di pers.), 22/66, es, habŭi, habitum, habēre, tr. 2; possi-
děo, possřdes, possēdi, possessum, possidēre, tr. 2; obtiněo,
obtines, obtinŭi, obtentum, obtinēre, tr. 2; si rende anche
con sum, es, fui, esse, intr. (col dat. di possesso); ho dei
libri, habeo libros o mihi sunt libri; avere in qualcuno
un amico, habēre aliquem amicum Cıc; avere qualcuno
come collega, habēre aliquem collegam Cīc; avere il
primo posto, principem locum obtinēre CEs; avere no-
me, soprannome, — nome, soprannome; avere per cer-
to, cognštum (0 explorātum o perspectum) habēre Cīc;
non avere, carčo, es, carŭi, part. fut. caritūrus, carēre,
intr. 2 (con l’abl.) b) (detto di qualità morali o intel-
lettuali), habēre; possidēre; praedštum (0 instructum o or-
 nātum) (am, um) esse (con l'abl.); si rende anche con
esse (0 inesse) con in e labil.: egli ha una grandissima
onestà, summa probïtas est in eo CIC; avere un animo
forte, animum fortem habēëre C1c c) (detto di senti-
menti), kabēre; avere odio per qualcuno, odium habēre
in aliquem d) (detto di rapporti o relazioni fra pers.),
utor, ulěris, usus sum, uti, intr. 3 dep. (con l’abl.); avere
familiarità con qualcuno, uti aliquo familiariter NEP;
avere qualcuno per maestro, uti aliquo magistro e en-
trare in possesso di, ottenere, adipiscor, adipiscěris,
adeptus sum, adipisci, tr. 3 dep. e impiegare, valersi
di, godere di, adhibčo, adhšïbes, adhibŭi, adhibtum, adhi-
bēre, tr. 2; uti (con l'abl.); avere (= mostrare) senso del-
la misura, modum adhibëre ©; avere riguardo, diligen-
` tiam adhibēre Cīc; aver buona salute, bonā valetudine
uti CEs; aver venti contrari, adversis ventis uti Cc e
soffrire, essere affetto da, affici (pass. di afficžo, afficis,
affeci, affectum, afficěre, tr. 3); labōro, as, āvi, ātum, āre,
intr. 1; avere una grave malattia, morbo gravi (abl.)
affectum esse o laborāre Cıīc; avere la febbre, febrem ha-
bēre Cīc e considerare, habēre; facšo, is, feci, factum,
facěre, tr. 3; avere in grande (in poco) conto qualcu-
no, magni (parvi) aliquem facěre e portare indosso, —

50

indossare e indosso © avere da, — dovere; non ho
niente da dire, nihil habeo quod dicam C10 e avere di
che (= aver motivo di), kabēre quod (col cong.); non
ho di che accusare la vecchiaia, nihil habeo quod accu-
sem senectutem CIC m avere vent'anni, vigesimum pri-
mum annum agëre (da ago, ?s, egi, actum, tr. 3); aversela
a male per qualcosa, moleste (0 aegre) ferre aliquid; ho
a che fare con qualcuno, res (0 consuetūdo) est mihi cum
aliquo; averne abbastanza, — abbastanza; avere la
peggio, vinci (pass. di vinco, is, vici, victum, vincěre,
tr. 3); avere (= generare) un figlio, filium procrëo, as,
āvi, ātum, āre. tr. 1; aver da fare, occupātum (am, um)
esse; non aver niente da fare, otiösum (am, um) esse. Le
locuz. formate da avere e un sost. (aver fame, sete,
sonno, fretta, paura, bisogno, ragione, €ecc.), si cer-
chino sotto i sost. (fame, sete, ecc.) o 1 verbi corrispon-
denti (aver inizio, — ¿niziare, cominciare; aver luogo,
— svolgersi, accadere, ecc.).

2. avere, averi, sost.: ricchezza, res, rei, f.; res fami-
liāris, rei familiāris, £., bona, ōrum, n. pl.; aumentare
gli averi, rem augēre Cc e credito, credštum; i, n.; li-
bro del dare e dell’avere, codex (gen. codšcis, m.) accep-
ti et expensi CIC.

Averno: Avernus, i, m.; dell Averno, Avernus, a, um;
lago di Averno, Avernus lacus, Averni lacūs, m.
avidamente: avžde; cupide.

avidità: avidštas, aviditātis, f., cupiditas, cupiditātis, £
avido: avždus ِ ) ¢, a, um (col gen.); esser avido
di denaro, pecuniam concupiscěre 1; essere avido di
onori, honores sitīre C10; essere avido di qualcosa, ali-
quid appetére Cc.

Avignone: Avennžo, Avenniōnis, £.; di Avignone,
Avennžcus, a, um.

avito: degli avi, avītus, a, um e antico, antiquus (pris-
cus), a, uM.

avo: avus, i, m.; gli avi, maiōres, um, m. pl.

avocare: (a sé), (ad se) revocāre (da revčco, as, āvi,
ātum, tr. 1); assumersi, suscipžo, suscšpis, suscapi, suscep-
tum, suscipěre, tr. 3.

avola: avža, ae, f.

avolo: avus, i, m.

avorio: ebur, eböris, n.; d'avorio, eburněus, a, um.
avulso: strappato, avulsus, a, um e separato, lonta-
no, remōtus, a, um (con a, ab e 11.

avvalersi: utor, utžris, usus sum, uti, intr. 3 dep. (con
l’abl.).

avvallamento: depressžo, depressiõnis, £., locus depres-
sus, loci depressi, m.

avvallarsi: subsīdo, is, subsēdi, subsessum, subsiděre,
intr. 3; depršmi (pass. di depršmo, is, depressi, depressum,
depriměre, tr. 3).

avvallato, agg.: depressus, a, UM.

avvalorare: firmo ( confirmo), as, āvi, ātum, āre, tr. l.
avvampare: flagro, as, āvi, ātum, āre, intr. l; exardes-
co, 1s, exarsi, exarsum, exardescěre, intr. 3.
avvantaggiare: prosum, prodes, profi, prodesse, intr.
(composto di sum) (col dat.); żuvo (adiŭvo), as, iūvi, iū-
tum (part. fut. iuvatūrus), iuvāre, tr. 1l avvantaggiar-
si: progredire, proficžo, profícis, profēci, profectum,
proficěre, intr. 3; trar vantaggio, utilitātem percipio, per-
cpis, percēpi, perceptum, percipěre, tr. 3.

avvedersi: animadverto, is, animadverti, animadver-
sum, animadvertěre, tr. 3, — anche accorgersi.
avvedutamente: prudenter; caute; callide.
avvedutezza: prudeníža, ae, f.; consiltum, úi, n

