#!/bin/bash

SOURCE=src/main.cr
OUT=bin/dictnorm
PUB=/home/nissl/Experiments/PROJECTS/latin-dic/latin-dic-lana

crystal build $SOURCE -o $OUT \
--release \
--verbose \
--link-flags "-flto"

cp $OUT $PUB/dictnorm -vf
#cp ./script/ocrall.sh $PUB/ocrall.sh -vf
#cp ./script/ocrgo.sh $PUB/ocrgo.sh -vf
#cp ./script/process.sh $PUB/process.sh -vf
#cp ./script/resamples.sh $PUB/resamples.sh -vf

#cp ./conf/markers.yml $PUB/markers.yml -vf
#cp ./conf/filters-A.yml $PUB/latin-letter-a/filters-A.yml -vf
#cp ./conf/filters-B.yml $PUB/latin-letter-b/filters-B.yml -vf

#cp ./script/process-A.sh $PUB/process-A.sh -vf
#cp ./script/process-B.sh $PUB/process-B.sh -vf
#cp ./script/process-testing.sh $PUB/process-testing.sh -vf

